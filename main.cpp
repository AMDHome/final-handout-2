#include "main.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <istream>

using namespace std;

HotelRooms::HotelRooms(const char* file)
{
	ifstream inf(file);
	string lines;
	
	string name;
	char discard;
	int number;
	int capacity;
	int occupants = 0;

	while(getline(inf, lines))
	{\
		stringstream line(lines);
		line >> number >> discard >> capacity >> discard;

		while(getline(line, name, ','))
		{
			resident.insert(pair<string, int> (name, number));
			occupants++;
		}

		rooms.insert(Room(number, capacity, occupants));
		number = capacity = occupants = 0;

	}
}

int HotelRooms::find(const string& nam)
{
	if(resident.find(nam) != resident.end())
	{
		return (resident.find(nam)->second);
	}
	else
		return 0;
}

ostream& operator<< (ostream &os, const RoomStats& rs)
{
	set <Room>::iterator itr;

	int empty = 0, occupied = 0, guests = 0, capacity = 0;
	for(itr = rs.rooms.begin(); itr != rs.rooms.end(); itr++)
	{
		if(itr->getOccupants())
		{
			occupied++;
			guests += itr->getOccupants();
		}
		else
			empty++;
		
		capacity += itr->getCapacity();
	}	
	cout << "Empty rooms: " << empty << " Occupied rooms: " << occupied
			 << " Guests: " << guests << " Capacity: " << capacity << endl;
}


int main()
{
  int choice, roomNumber;
  HotelRooms rooms("rooms.csv");


  do{
    cout << "\nMenu\n0. Done\n1. Find guest\n2. Print totals.\nYour choice: ";
    cin >> choice;
    if(choice == 1)
    {
     string name;
     cout << "Guest name: ";
     cin.get();
     getline(cin,name);
     roomNumber = rooms.find(name);
     if(roomNumber)
       cout << name << " is in Room #" << roomNumber << endl;
     else
        cout << name << " is not staying here . \n";
    }
  else
    if(choice == 2)
      cout << "Hotel totals \n" << rooms << "End of list\n";
  }while(choice);
  return(0);
}

