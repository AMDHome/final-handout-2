#ifndef MAIN_H
#define MAIN_H
#include <set>
#include <fstream>
#include <map>
#include <iostream>
#include <string>

using namespace std;

class Room
{
  const short number;
  const short capacity;
  short occupants;
  public:
  Room(short n, short c, short oc) : number(n), capacity(c), occupants(oc){}
  short getNumber()const{return number;}
  short getCapacity() const{return capacity;}
  short getOccupants() const{return occupants;}
  bool operator< (const Room &rhs) const {return number < rhs.number;}
};

class RoomStats{

protected:
  set <Room> rooms;
public:
  friend ostream& operator<< (ostream &os, const RoomStats&rs);

};




class HotelRooms: public RoomStats
{
 public:

   multimap<string, int> resident;

   HotelRooms(const char* file);
   int find(const string& nam);

  /*void& operator()(ifstream inf)
  {
    vector<string> data;
    ifstream in(inf)
    vector<string> data;
    string s;
    while(getline(in,s))
    {
      stringstream ss(s);
      while(ss)
      {
        if(!getline(ss,s,','))
          break;

        data.push_back(s);
      }
    }
  }*/


};






#endif
